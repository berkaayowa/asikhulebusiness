<?php
	namespace Controller\Client;
	use BerkaPhp\Controller\BerkaPhpController;
    use BerkaPhp\Helper\Debug;
    use BrkORM\T;
    use Helper\Check;


    class ServicesController extends BerkaPhpController
	{

		function __construct() {
			parent::__construct(false);
            $this->view->set('menuTitle', 'Our Services');
		}

        /* Display all users from database
        *  Client action in this controller
        *  @author berkaPhp
        */

		function index() {

            $services = T::Find('service')->Where('isDeleted', '=', Check::$False)->FetchList();
            $this->view->set('services', $services);
			$this->view->render();

		}

	}

?>