<?php
	namespace Controller\Client;
	use BerkaPhp\Controller\BerkaPhpController;
    use BerkaPhp\Helper\Debug;
    use BrkORM\T;
    use Helper\Check;


    class GalleryController extends BerkaPhpController
	{

		function __construct() {
			parent::__construct(false);
            $this->view->set('menuTitle', 'Our Gallery');
		}

        /* Display all users from database
        *  Client action in this controller
        *  @author berkaPhp
        */

		function index() {

            $images = T::Find('gallery')
                ->Join('service', 'service.id = gallery.refServiceId')
                ->Where('gallery.isDeleted', '=', Check::$False)
                ->FetchList();
            $this->view->set('images', $images);
			$this->view->render();

		}

	}

?>