<?php
	namespace Controller\Client;
	use BerkaPhp\Controller\BerkaPhpController;
    use BerkaPhp\Helper\Debug;
    use BrkORM\T;
    use Helper\Check;


    class ContactController extends BerkaPhpController
	{

        private $mailer;

		function __construct() {
			parent::__construct(false);
            $this->view->set('menuTitle', 'Contact Us');
            $this->mailer = $this->loadComponent("Email");
		}

        /* Display all users from database
        *  Client action in this controller
        *  @author berkaPhp
        */

		function index() {

            $contact = T::Find('contact')->Where('isDeleted', '=', Check::$False)->FetchFirstOrDefault();
            $this->view->set('contacts', $contact);
			$this->view->render();

		}

        function message() {

            $data = $this->getPost();

            if(sizeof($data) > 0) {

                $ourEmail = SUPPORT_EMAIL;

                $emailContent = "Hi,<br><br>You have new message from the contact form below are the details<br><br>";
                $emailContent = $emailContent."Name: ".ucfirst($data["name"])."<br>Phone: ".$data["phone"].'<br>Email: '.$data["email"].'<br><br>';
                $emailContent = $emailContent."".$data["message"]."";

                $isSent = $this->mailer->send($_SERVER['SERVER_NAME'], ucfirst($_SERVER['SERVER_NAME']) . ' - New Message', "", $emailContent, $ourEmail);

                if($isSent)
                    return $this->jsonFormat(['success'=>true,'error'=> false, 'message'=> "Your message has been sent to us successfully."]);
                else
                    return $this->jsonFormat(['success'=>false,'error'=> true, 'message'=> "Your message could not be sent to us, try again or email us using our contacts information"]);

            }

        }

	}

?>