<?php
	namespace Controller\Client;
	use BerkaPhp\Controller\BerkaPhpController;
    use BerkaPhp\Helper\Debug;
    use BerkaPhp\Helper\RedirectHelper;
    use BerkaPhp\Helper\SessionHelper;
    use BrkORM\T;
    use Helper\Check;

    class UsersController extends BerkaPhpController
	{

		function __construct() {
			parent::__construct(false);
            $this->view->set('menuTitle', 'Members');
		}

        /* Display all users from database
        *  Client action in this controller
        *  @author berkaPhp
        */

		function index() {
			RedirectHelper::redirect('/users/login');
		}

        function login() {

            if(sizeof($this->getPost()) > 0) {

                $user = T::Find('user')
                    ->Join('role', 'role.Id = user.refRoleId')
                    ->Join('status', 'status.Id = user.refStatusId')
                    ->Where('user.username' , '=', $this->getPost()['username'])
                    ->Where('user.password' , '=',  md5($this->getPost()['password']))
                    ->Where('user.isDeleted', '=', 0)
                    ->FetchFirstOrDefault();

                if ($user->IsAny()) {

                    if($user->status->code == 'PNC') {

                        if (SessionHelper::exist('user')) {
                            SessionHelper::remove('user');
                        }

                        SessionHelper::add('user', serialize($user));

                        sleep(1);
                        return $this->jsonFormat(['success'=>true,'error'=> false, 'message'=> "Successfully logged in", 'link'=>'/admin/users']);
                    } else if($user->status->code == 'PFC' || $user->isPhoneNumberConfirmed == Check::$False) {
                        return $this->jsonFormat(['success'=>false,'error'=> true, 'message'=> 'Your account is not verified yet']);
                    } else if($user->status->code == 'APD') {
                        return $this->jsonFormat(['success'=>false,'error'=> true, 'message'=> 'Your account has been suspended, contact support']);
                    } else {
                        return $this->jsonFormat(['success'=>false,'error'=> true, 'message'=> 'Your account status is invalid, contact support']);
                    }

                } else {
                    return $this->jsonFormat(['success'=>false,'error'=> true, 'message'=> 'Invalid username or password']);
                }
            }

            $this->view->set('title', 'Sign in');

            $this->view->render();
        }

        function logout() {
            SessionHelper::remove('user');
            SessionHelper::kill();
            RedirectHelper::redirect('/users/login');
        }

        function unauthorized() {
            $this->view->render();
        }


    }

?>