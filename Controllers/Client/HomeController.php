<?php
	namespace Controller\Client;
	use BerkaPhp\Controller\BerkaPhpController;
    use BerkaPhp\Helper\Debug;
    use BrkORM\T;


    class HomeController extends BerkaPhpController
	{

		function __construct() {
			parent::__construct(false);
            $this->view->set('menuTitle', 'Home');
		}

        /* Display all users from database
        *  Client action in this controller
        *  @author berkaPhp
        */

		function index() {

			$this->view->render();

		}

	}

?>