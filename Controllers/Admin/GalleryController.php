<?php
	namespace Controller\Admin;
	use BerkaPhp\Controller\BerkaPhpController;
    use BerkaPhp\Helper\Debug;
    use BerkaPhp\Helper\Html;
    use BerkaPhp\Helper\Rand;
    use BrkORM\T;
    use Helper\Check;

    class GalleryController extends BerkaPhpController
	{

		function __construct() {
			parent::__construct(false);

		}

        /* Display all users from database
        *  Client action in this controller
        *  @author berkaPhp
        */

		function index() {

            $gallery = T::Find('gallery')
                ->Join('service', 'service.id = gallery.refServiceId')
                ->Where('gallery.isDeleted', '=', Check::$False)
                ->FetchList();

			$this->view->set('galleries', $gallery);
			$this->view->render();

		}

        function add() {

            $data = $this->getPost();

            if(sizeof($data) > 0) {

                $gallery = T::Create('gallery');
                $gallery->SetProperties($data);

                if(sizeof($_FILES) > 0) {


                    $fileName = "image";

                    if ($_FILES[$fileName]['error'] !== UPLOAD_ERR_OK) {
                        return $this->jsonFormat(['success' => false, 'message' => '!oop file error', 'error' => true]);
                    }

                    $extension = 'png';

                    $mime = $_FILES[$fileName]['type'];

                    $identity = Rand::uniqueDigit(200, 10000000);


                    $file_name = $identity . '.' .$extension;

                    $isUploaded = @move_uploaded_file($_FILES[$fileName]['tmp_name'],"Views/Asset/Images/".$file_name);


                    if ($isUploaded) {
                        $gallery->image = $file_name;

                        if ($gallery->Save()) {
                            sleep(1);
                            return $this->jsonFormat(['error' => false, 'message' => 'Gallery has been added successfully', 'success' => true]);
                        } else {
                            return $this->jsonFormat(['error'=>true, 'message'=>'Error could not added gallery' ,'success'=>false]);
                        }

                    }
                    else
                        return $this->jsonFormat(['success' => true, 'message' => 'image could not upload successfully', 'error' => false]);
                } else {
                    return $this->jsonFormat(['success' => true, 'message' => 'image was not provided', 'error' => false]);
                }



            }

            $services = T::Find('service')
                ->Where('isDeleted', '=', Check::$False)
                ->FetchList(['assocArray'=>true]);

            $this->view->set('services', $services);

            $this->view->render();
        }

        function edit($option) {

            $data = $this->getPost();

            $gallery = T::Find('gallery')
                ->Join('service', 'service.id = gallery.refServiceId')
                ->Where('gallery.id', '=', $option['args']['params'][0])
                ->Where('gallery.isDeleted', '=', Check::$False)
                ->FetchFirstOrDefault();

            if(sizeof($data) > 0) {

                if(!$gallery->IsAny())
                    return $this->jsonFormat(['error'=>true, 'message'=>'Opps! could no find this gallery' ,'success'=>false]);

                $gallery->SetProperties($data);

                if(sizeof($_FILES) > 0) {


                    $fileName = "image";

                    if ($_FILES[$fileName]['error'] !== UPLOAD_ERR_OK) {
                        return $this->jsonFormat(['success' => false, 'message' => '!oop file error', 'error' => true]);
                    }

                    $extension = 'png';

                    $mime = $_FILES[$fileName]['type'];

                    $identity = Rand::uniqueDigit(200, 10000000);


                    $file_name = $identity . '.' .$extension;

                    $isUploaded = @move_uploaded_file($_FILES[$fileName]['tmp_name'],"Views/Asset/Images/".$file_name);


                    if ($isUploaded) {
                        $gallery->image = $file_name;

                        if ($gallery->Save()) {
                            sleep(1);
                            return $this->jsonFormat(['error' => false, 'message' => 'Gallery has been updated successfully', 'success' => true]);
                        } else {
                            return $this->jsonFormat(['error'=>true, 'message'=>'Error could not update gallery' ,'success'=>false]);
                        }

                    }
                    else
                        return $this->jsonFormat(['success' => true, 'message' => 'image could not upload successfully', 'error' => false]);
                } else {


                    if ($gallery->Save()) {
                        sleep(1);
                        return $this->jsonFormat(['error' => false, 'message' => 'Gallery has been updated successfully', 'success' => true]);
                    } else {
                        return $this->jsonFormat(['error'=>true, 'message'=>'Error could not update gallery' ,'success'=>false]);
                    }

                }

            }

            $services = T::Find('service')
                ->Where('isDeleted', '=', Check::$False)
                ->FetchList(['assocArray'=>true]);

            $this->view->set('services', $services);
            $this->view->set('gallery', $gallery);

            $this->view->render();
        }

        function delete($option) {

            $gallery = T::Find('gallery')
                ->Where('id', '=', $option['args']['params'][0])
                ->Where('isDeleted', '=', Check::$False)
                ->FetchFirstOrDefault();

            if(!$gallery->IsAny())
                return $this->jsonFormat(['error'=>true, 'message'=>'Opps! could no find this gallery' ,'success'=>false]);

            $gallery->isDeleted = Check::$True;

            if ($gallery->Save()) {
                sleep(1);
                return $this->jsonFormat(['error' => false, 'message' => 'Gallery has been deleted successfully', 'success' => true,'redirect'=> Html::action('/gallery')]);
            } else {
                return $this->jsonFormat(['error'=>true, 'message'=>'Error could not delete gallery' ,'success'=>false]);
            }


        }


    }

?>