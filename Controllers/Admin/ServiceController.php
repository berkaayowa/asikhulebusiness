<?php
	namespace Controller\Admin;
	use BerkaPhp\Controller\BerkaPhpController;
    use BerkaPhp\Helper\Debug;
    use BerkaPhp\Helper\Html;
    use BerkaPhp\Helper\Rand;
    use BrkORM\T;
    use Helper\Check;

    class ServiceController extends BerkaPhpController
	{

		function __construct() {
			parent::__construct(false);

		}

        /* Display all users from database
        *  Client action in this controller
        *  @author berkaPhp
        */

		function index() {

            $services = T::Find('service')
                ->Where('isDeleted', '=', Check::$False)
                ->FetchList();

			$this->view->set('services', $services);
			$this->view->render();

		}

        function add() {

            $data = $this->getPost();

            if(sizeof($data) > 0) {

                $service = T::Create('service');
                $service->SetProperties($data);

                if ($service->Save()) {
                    sleep(1);
                    return $this->jsonFormat(['error'=>false, 'message'=>'service has been added successfully', 'success'=>true]);
                } else {
                    return $this->jsonFormat(['error'=>true, 'message'=>'Error could not added service' ,'success'=>false]);
                }

            }

            $this->view->render();
        }

        function image($option) {

            $data = $this->getPost();

            $service = T::Find('service')
                ->Where('isDeleted', '=', Check::$False)
                ->Where('id', '=', $option['args']['params'][0])
                ->FetchFirstOrDefault();

            if(sizeof($data) > 0) {

                if(!$service->IsAny())
                    return $this->jsonFormat(['error'=>true, 'message'=>'Opps! could no find this service' ,'success'=>false]);

                $service->SetProperties($data);

                if ($service->Save()) {
                    sleep(1);
                    return $this->jsonFormat(['error'=>false, 'message'=>'Service as been updated successfully', 'success'=>true]);
                } else {
                    return $this->jsonFormat(['error'=>true, 'message'=>'Error could not updated service' ,'success'=>false]);
                }

            }

            $this->view->set('service', $service);

            $this->view->render();
        }

        function edit($option) {

            $data = $this->getPost();

            $service = T::Find('service')
                ->Where('isDeleted', '=', Check::$False)
                ->Where('id', '=', $option['args']['params'][0])
                ->FetchFirstOrDefault();

            if(sizeof($data) > 0) {

                if(!$service->IsAny())
                    return $this->jsonFormat(['error'=>true, 'message'=>'Opps! could no find this service' ,'success'=>false]);

                $service->SetProperties($data);

                $save = $service->Save();

                if ($save || sizeof($_FILES) > 0) {

                    if(sizeof($_FILES) > 0) {


                        $fileName = "image";

                        if ($_FILES[$fileName]['error'] !== UPLOAD_ERR_OK) {
                            return $this->jsonFormat(['success' => false, 'message' => '!oop file error', 'error' => true]);
                        }

                        $extension = 'png';

                        $mime = $_FILES[$fileName]['type'];

                        $identity = Rand::uniqueDigit(200, 10000000);


                        $file_name = $identity . '.' .$extension;

                        $isUploaded = @move_uploaded_file($_FILES[$fileName]['tmp_name'],"Views/Asset/Images/".$file_name);

                        if ($isUploaded) {
                            $service->image = $file_name;
                            $service->Save();

                            sleep(1);
                            return $this->jsonFormat(['success' => true, 'message' => ($save ? 'Service has been updated and ' : 'Service could not be updated, ' ).' image Uploaded successfully', 'error' => false]);
                        }
                        else
                            return $this->jsonFormat(['success' => true, 'message' => ($save ? 'Service has been updated and ' : 'Service could not be updated, ' ).' image could not upload successfully', 'error' => false]);
                    }

                    return $this->jsonFormat(['error'=>false, 'message'=>'Service has been updated successfully', 'success'=>true]);
                } else {
                    return $this->jsonFormat(['error'=>true, 'message'=>'Error could not updated service' ,'success'=>false]);
                }

            }

            $this->view->set('service', $service);

            $this->view->render();
        }

        function delete($option) {

            $service = T::Find('service')
                ->Where('id', '=', $option['args']['params'][0])
                ->Where('isDeleted', '=', Check::$False)
                ->FetchFirstOrDefault();

            if(!$service->IsAny())
                return $this->jsonFormat(['error'=>true, 'message'=>'Opps! could no find this service' ,'success'=>false]);

            $service->isDeleted = Check::$True;

            if ($service->Save()) {
                sleep(1);
                return $this->jsonFormat(['error' => false, 'message' => 'service has been deleted successfully', 'success' => true,'redirect'=> Html::action('/service')]);
            } else {
                return $this->jsonFormat(['error'=>true, 'message'=>'Error could not delete service' ,'success'=>false]);
            }


        }


    }

?>