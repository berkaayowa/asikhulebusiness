<?php
	namespace Controller\Admin;
	use BerkaPhp\Controller\BerkaPhpController;
    use BerkaPhp\Helper\Debug;
    use BrkORM\T;

    class UsersController extends BerkaPhpController
	{

		function __construct() {
			parent::__construct(false);

		}

        /* Display all users from database
        *  Client action in this controller
        *  @author berkaPhp
        */

		function index() {

            $users = T::Find('user')
                ->Join('role', 'role.Id = user.refRoleId')
                ->Join('status', 'status.Id = user.refStatusId')
                ->Where('user.isDeleted', '=', 0)
                ->FetchList();

			$this->view->set('users', $users);
			$this->view->render();

		}

        function add() {

            $data = $this->getPost();

            if(sizeof($data) > 0) {

                $user = T::Create('user');
                $user->SetProperties($data);
                $user->username = $user->mobileNumber;
                $user->password = md5($user->password);

                if ($user->Save()) {
                    sleep(1);
                    return $this->jsonFormat(['error'=>false, 'message'=>'User as been added successfully', 'success'=>true]);
                } else {
                    return $this->jsonFormat(['error'=>true, 'message'=>'Error could not added user' ,'success'=>false]);
                }

            }

            $roles = T::Find('role')->FetchList(['assocArray'=>true]);
            $status = T::Find('status')->FetchList(['assocArray'=>true]);

            $this->view->set('roles', $roles);
            $this->view->set('status', $status);

            $this->view->render();
        }

        function edit($option) {

            $data = $this->getPost();

            $user = T::Find('user')
                ->Join('role', 'role.Id = user.refRoleId')
                ->Join('status', 'status.Id = user.refStatusId')
                ->Where('user.id', '=', $option['args']['params'][0])
                ->FetchFirstOrDefault();

            if(sizeof($data) > 0) {

                if(!$user->IsAny())
                    return $this->jsonFormat(['error'=>true, 'message'=>'Opps! could no find this user' ,'success'=>false]);

                $user->SetProperties($data);

                if ($user->Save()) {
                    sleep(1);
                    return $this->jsonFormat(['error'=>false, 'message'=>'User as been updated successfully', 'success'=>true]);
                } else {
                    return $this->jsonFormat(['error'=>true, 'message'=>'Error could not updated user' ,'success'=>false]);
                }

            }

            $roles = T::Find('role')->FetchList(['assocArray'=>true]);
            $status = T::Find('status')->FetchList(['assocArray'=>true]);

            $this->view->set('user', $user);

            $this->view->set('roles', $roles);
            $this->view->set('status', $status);

            $this->view->render();
        }


    }

?>