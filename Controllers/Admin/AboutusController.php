<?php
	namespace Controller\Admin;
	use BerkaPhp\Controller\BerkaPhpController;
    use BerkaPhp\Helper\Debug;
    use BrkORM\T;
    use Helper\Check;

    class AboutusController extends BerkaPhpController
	{

		function __construct() {
			parent::__construct(false);
		}

        /* Display all users from database
        *  Client action in this controller
        *  @author berkaPhp
        */

		function index() {

            $about = T::Find('about')
                ->Where('isDeleted', '=', Check::$False)
                ->FetchFirstOrDefault();

            $this->view->set('about', $about);
			$this->view->render();

		}

        function edit($option){

            $data = $this->getPost();

            $about = T::Find('about')
                ->Where('isDeleted', '=', Check::$False)
                ->Where('id', '=', $option['args']['params'][0])
                ->FetchFirstOrDefault();

            if (sizeof($data) > 0) {

                if (!$about->IsAny())
                    return $this->jsonFormat(['error' => true, 'message' => 'Opps! could not find about', 'success' => false]);

                $about->SetProperties($data);

                if ($about->Save()) {
                    sleep(1);
                    return $this->jsonFormat(['error' => false, 'message' => 'About us has been updated successfully', 'success' => true]);
                } else {
                    return $this->jsonFormat(['error' => true, 'message' => 'Error could not updated About us', 'success' => false]);
                }

            }
        }


    }

?>