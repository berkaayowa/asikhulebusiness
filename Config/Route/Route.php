<?php
namespace Config\Router;
use BerkaPhp\Helper\Auth;
use BerkaPhp\Helper\RedirectHelper;
use BerkaPhp\Helper\SessionHelper;
use BerkaPhp\Router\Dispatcher\Route;

/**
 * Created by PhpStorm.
 * User: f
 * Date: 2018-07-13
 * Time: 2:44 AM
 * @param $router
 */
class Router {


    public static function Setup($router){

        /**
         * Created by PhpStorm.
         * User: Berka
         * Date: 2018-07-13
         * Time: 2:44 AM
         * setup prefix
         */
        $router->prefix
        (
            [
                'client'=>[
                    'name'=>'Client',
                    'home'=>'home'
                ],
                'admin'=>[
                    'name'=>'Admin',
                    'home'=>'users'
                ]
            ],
            ['default'=>'client']
        );

        /**
         * Created by PhpStorm.
         * User: Berka
         * Date: 2018-07-13
         * Time: 2:44 AM
         * setup routing
         */

        $router->route('/', function ($route) {

            $requiredLogin = ['Admin'];

            if(in_array($route['prefix']['name'], $requiredLogin)) {

                if (Auth::IsUserLogged()) {

                    if(!in_array(Auth::GetActiveUser(false)->role->code, ['ADM'])) {
                        RedirectHelper::redirect('/users/unauthorized');
                    }

                } else {
                    RedirectHelper::redirect('/users/login');
                }

            }

            Route::to($route);

        });


        return $router;

    }
}

?>