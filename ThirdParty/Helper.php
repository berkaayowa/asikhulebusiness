<?php
/**
 * Created by PhpStorm.
 * User: f
 * Date: 2018-06-13
 * Time: 6:46 PM
 */

namespace Util;

class Helper {

    public static function select($id,  $data = array(), $options) {

        $i_select= "<select id='{$id}' name='{$id}' ";
        $i_option = '';
        $selected = isset($options['selected']) ? $options['selected'] : '';

        $length = sizeof($data);
        $option_length = sizeof($options);

        if($option_length > 0) {

            foreach($options as $_option => $values) {
                if($_option != 'selected' && $_option != 'value' && $_option != 'text' ) {
                    $i_select.=" {$_option} ='{$values}''";
                }
            }

        }

        if($length > 0) {

            foreach($data as $data_option) {

                $sel = '';

                $data_option = (array) $data_option;

                if($selected == $data_option[$options['value']])
                {
                    $sel = 'selected';
                }
                $i_option.='<option '.$sel.' value ='.$data_option[$options['value']].'>'.$data_option[$options['text']].'</option>';
            }

        } else {
            $i_option.='<option></option>';
        }

        return $i_select.'>'.$i_option.'</select>';
    }

    public static function GetCurrentUrl() {
        return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }

    public static function SendSMS($body = array()) {

    }



} 