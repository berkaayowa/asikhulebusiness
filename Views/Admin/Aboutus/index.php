
<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="panel-title">
                <div class="title-box">
                    <label class="text-muted ">About us</label>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <form data-toggle="validator" message="Updating..." request-type="POST" id="formUser" data-request="<?= BerkaPhp\Helper\Html::action('/aboutus/edit/'.$about->id)?>">
            <div class="row">
                <div class="col-md-6 hidden">

                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="title">Title:</label>
                        <input required type="text" class="form-control" name="title" id="title" value="<?=$about->title?>">
                    </div>
                    <div class="form-group">
                        <label for="summary">Summary:</label>
                        <textarea rows="5" required class="form-control" name="summary" id="summary"><?=$about->summary?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="content">Content:</label>
                        <textarea required data-editor class="form-control" name="content" id="content"><?=$about->content?></textarea>
                    </div>
                </div>
            </div>
            <input type="hidden" id="id" name="id" value="<?=$about->id?>"/>
            <button type="submit" class="btn btn-success">Update </button>
        </form>
    </div>
</div>