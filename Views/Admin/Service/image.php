
<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="title-box">
                <label class="text-muted pull-left"><span class="badge title">Updating service</span></label>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <form data-toggle="validator" message="Updating..." request-type="POST" id="formUser" data-request="<?= BerkaPhp\Helper\Html::action('/service/edit/'.$service->id)?>">
            <div class="row">

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input required type="text" class="form-control" name="name" id="name" value="<?=$service->name?>">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="">
                                    <label >Image:</label><br/>
                                    <figure>
                                        <img id="servicePreview" src="/Views/Asset/Images/slider_4.jpg" alt="Free Template by uicookies.com" class="img-responsive">
                                    </figure>
                                </div>
                                <div class="input-group">
                                    <input data-image-cropper="#servicePreview" type="file" class="form-control" identity="image">
                                    <div class="input-group-addon">
                                        <i class="fa fa-paperclip"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="btn btn-default">Rotate Right</div>
                            <div class="btn btn-default">Rotate Left</div>
                            <button type="submit" class="btn btn-success">Crop & Upload </button>
                        </div>
                    </div>
                </div>

            </div>
            <input type="hidden" id="id" name="id" value="<?=$service->id?>"/>
        </form>
    </div>
</div>