
<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="title-box">
                <label class="text-muted pull-left"><span class="badge title">Services</span></label>
                <div class="btn-group pull-right">
                    <a href="<?= BerkaPhp\Helper\Html::action('/service/add') ?>" type="button" class="btn btn-default">
                        New service
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table" id="dataTable">
                <thead class="thead-inverse">
                    <tr>
                        <th>#</th>
                        <th>name</th>
                        <th>summary</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($services as $service) :?>
                    <tr>
                        <td><?=$service->id?></td>
                        <td><?=$service->name?></td>
                        <td><?= \BerkaPhp\Helper\Text::limitChar($service->summary, 75, '...') ?></td>
                        <td>
                            <a href="<?= BerkaPhp\Helper\Html::action('/service/edit/'.$service->id) ?>">
                                <span class="label label-default">Manage</span>
                            </a>
                            <span class="label label-default">
                                <a data-action-btn="<?= BerkaPhp\Helper\Html::action('/service/delete/'.$service->id) ?>" confirmation-title="Delete Confirmation" confirmation-message="Are you sure you want to delete ?" loading="Deleting...">
                                    Delete
                                </a>
                            </span>
                        </td>
                    </tr>
                <?php endforeach?>
                </tbody>
            </table>
        </div>
    </div>
</div>