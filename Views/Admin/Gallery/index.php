
<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="title-box">
                <label class="text-muted pull-left"><span class="badge title">Gallery</span></label>
                <div class="btn-group pull-right">
                    <a href="<?= BerkaPhp\Helper\Html::action('/gallery/add') ?>" type="button" class="btn btn-default">
                        New image
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table" id="dataTable">
                <thead class="thead-inverse">
                    <tr>
                        <th>#</th>
                        <th>service</th>
                        <th>image</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($galleries as $gallery) :?>
                    <tr>
                        <td><?=$gallery->id?></td>
                        <td><?=$gallery->service->name?></td>
                        <td><?=$gallery->image?></td>
                        <td>
                            <a href="<?= BerkaPhp\Helper\Html::action('/gallery/edit/'.$gallery->id) ?>">
                                <span class="label label-default">Manage</span>
                            </a>
                            <span class="label label-default">
                                <a data-action-btn="<?= BerkaPhp\Helper\Html::action('/gallery/delete/'.$gallery->id) ?>" confirmation-title="Delete Confirmation" confirmation-message="Are you sure you want to delete ?" loading="Deleting...">
                                    Delete
                                </a>
                            </span>
                        </td>
                    </tr>
                <?php endforeach?>
                </tbody>
            </table>
        </div>
    </div>
</div>