<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="title-box">
                <label class="text-muted pull-left"><span class="badge title">New gallery image</span></label>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <form data-toggle="validator"  message="Adding new image..." request-type="POST" id="formUser" data-request="<?= BerkaPhp\Helper\Html::action('/gallery/add')?>">
            <div class="row">
                <div class="col-md-7">
                    <div class="form-group">
                        <div class="">
                            <label >Image:</label><br/>
                            <figure>
                                <img id="servicePreview" src="/Views/Asset/Images/slider_4.jpg" alt="Free Template by uicookies.com" class="img-responsive">
                            </figure>
                        </div>
                        <div class="input-group">
                            <input data-image-cropper="#servicePreview" type="file" class="form-control" identity="image">
                            <div class="input-group-addon">
                                <i class="fa fa-paperclip"></i>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-5">
                    <div class="form-group">
                        <label for="refRoleId">Related Service</label>
                        <?= Util\Helper::select('refServiceId', $services, ['value'=>'id', 'text'=>'name', 'class'=>'form-control']) ?>
                    </div>
                    <div class="form-group">
                        <label for="description">Description:</label>
                        <textarea rows="10" required class="form-control" name="description" id="description"></textarea>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <span class="btn btn-default" style="">Rotate Right</span>
                            <span class="btn btn-default" style="">Rotate Left</span>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <button type="submit" class="btn btn-success">Save</button>
        </form>
    </div>
</div>