<!DOCTYPE html>
<html lang="en">
<head>
    <?= $meta_data ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= empty($title) ? 'Green Valley Design' : ucfirst($title) ?></title>

    <link rel="stylesheet" type="text/css" href="/Views/Admin/Layout/css/bootstrap.min.css">
    <link rel="stylesheet" href="/Views/Admin/Layout/css/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" href="/Views/Shared/Css/FontAwesome/font-awesome.css"/>
    <link rel="stylesheet" href="/Views/Shared/Css/Other/croppie.css"/>
    <link rel="stylesheet" href="/Views/Admin/Layout/css/styles.css" />
    <link rel="stylesheet" href="/Views/Admin/Layout/css/style.css" />
    <link rel="stylesheet" href="/Views/Admin/Layout/css/bootstrap-imageupload.css" />
    <link rel="stylesheet" href="/Views/Admin/Layout/css/customer.css" />
    <link rel="shortcut icon" href="/Views/Asset/icon.png" type="image/x-icon">

</head>
<body>
<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="logo">
                    <a href="/" target="_blank">
                        <img src="/Views/Asset/logo.png" class="logo-img" alt=""/>
                    </a>
                </div>
            </div>
            <div class="col-md-5">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group form">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="navbar navbar-inverse" role="banner">
                    <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">

                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Hi <?= ucfirst( BerkaPhp\Helper\Auth::GetActiveUser()->name)?> <b class="caret"></b></a>
                                <ul class="dropdown-menu animated fadeInUp">
                                    <li><a href="/">Profile</a></li>
                                    <li><a href="/users/logout">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page-content">
    <div class="container">
        <div class="col-md-3">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <li class="current"><a href="<?= BerkaPhp\Helper\Html::action('/') ?>"><i class="glyphicon glyphicon-home"></i>Home</a></li>
                    <li><a href="<?= BerkaPhp\Helper\Html::action('/aboutus') ?>"><i class="glyphicon glyphicon-info-sign"></i>About us</a></li>
                    <li><a href="<?= BerkaPhp\Helper\Html::action('/contact') ?>"><i class="glyphicon glyphicon-phone"></i>Contacts</a></li>
                    <li><a href="<?= BerkaPhp\Helper\Html::action('/service') ?>"><i class="glyphicon glyphicon-list"></i>Services</a></li>
                    <li><a href="<?= BerkaPhp\Helper\Html::action('/gallery') ?>"><i class="glyphicon glyphicon-blackboard"></i>Gallery</a></li>
                </ul>
            </div>

            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <li><a href="<?= BerkaPhp\Helper\Html::action('/users') ?>"><i class="glyphicon glyphicon-user"></i>Users</a></li>
                    <li><a href="/users/logout"><i class="glyphicon glyphicon-log-out"></i>logout</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="">
                    {content}
                </div>
            </div>
        </div>
    </div>
</body>
</html>

<script src="/Views/Admin/Layout/js/jquery-2.1.1.js"></script>
<script src="/Views/Client/Layout/js/bootstrap.min.js"></script>
<script src="/Views/Shared/Scripts/Ckeditor/ckeditor.js"></script>
<script src="/Views/Shared/Scripts/Other/croppie.js"></script>
<script src="/Views/Admin/Layout/js/loader.js"></script>
<script src="/Views/Admin/Layout/js/confirmation.js"></script>
<script src="/Views/Admin/Layout/js/notification.js"></script>
<script src="/Views/Admin/Layout/js/Jquery_table.js"></script>
<script src="/Views/Admin/Layout/js/datatables.js"></script>
<script src="/BerkaPhp/Template/Utility/Javascript/app.js"></script>
<script src="/Views/Admin/Layout/js/app.js"></script>









