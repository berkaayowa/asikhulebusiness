/**
 * Created by berka on 2017/12/31.
 */
var mts = {};

mts.initLogin = function (){

    $('#login-form').validate({
        rules:{
            Email:{required:true},
            Password:{required:true}
        },
        messages: {
            Email:{required:'Email is required'},
            Password:{required:'Password is required'}
        },
        submitHandler: function(form) {

            return true;
        }
    });

    $('#login-form').validate({
        rules:{
            Email:{required:true},
            Password:{required:true}
        },
        messages: {
            Email:{required:'Email is required'},
            Password:{required:'Password is required'}
        },
        submitHandler: function(form) {

            return true;
        }
    });
};

mts.initSignup = function (){

    $('#register-form').validate({
        rules:{
            FirstName:{required:true},
            LastName:{required:true},
            Email:{required:true},
            Password:{required:true, minlength: 5},
            ConfirmPassword:{required:true, minlength: 5, equalTo: "#Password-register"}
        },
        messages: {
            FirstName:{required:'First name is required'},
            LastName:{required:'Last name is required'},
            Email:{required:'Email address is required'},
            Password:{required:'Password is required'},
            ConfirmPassword:{required:'Please confirm your password', equalTo:'Passwords does not match!'}
        },
        submitHandler: function(form) {

            $('#ajaxLoader').loader('setMessage', 'Registering...').loader('show');

            var data = {};

            $('#register-form input').each(function() {
                data[$(this).attr('name')] = $(this).val();
            });

            berkaPhpJs.request({
                url:'/users/signup',
                type: 'POST',
                data: data,
                message: 'Registering...'
            }, function(success, result) {

                if(result.success) {
                    $('#register-form input').each(function() {
                        $(this).val('');
                    });

                    $('#information').modal('show');
                }

            });

            return false;
        }
    });
};

mts.initFormRequest = function (){

    $('[data-request]').each(function() {

        $(this).on('submit', function(e) {

            e.preventDefault();

            var url = $(this).data('request');
            var type = $(this).attr('request-type');
            var message = $(this).attr('message');
            var id  = $(this).attr('id');
            var responseType = $(this).attr('response-type');
            var responseOn = $(this).attr('response-on');

            var formId = $(this).attr('id');

            var data = {};

            var form = $(this);
            var formdata = false;
            if (window.FormData){

                formdata = new FormData(form[0]);

                var length = $('#' + formId +' [data-image-cropper]').length;
                var count = 0;

                if(length == 0) {
                    berkaPhpJs.request({
                        url: url,
                        type: type,
                        data: formdata ? formdata : form.serialize(),
                        hasFile: true,
                        message: message
                    }, function(success, result) {

                        var s = result;

                        if(result['link'] != null)
                            window.location = result['link'];

                        if(responseType == 'html' && result['content']) {
                            $(responseOn).html(result['content'])
                            return ;
                        }

                        if(result.success) {

                        }

                    });
                } else {

                    $('#' + formId +' [data-image-cropper]').each(function() {
                        var id = $(this).attr('identity');
                        var element = $(this).data('image-cropper');
                        $(element).croppie('result', 'blob').then(function (data) {
                            formdata.append(id, data, id +'.png');

                            if(count == length - 1) {
                                berkaPhpJs.request({
                                    url: url,
                                    type: type,
                                    data: formdata ? formdata : form.serialize(),
                                    hasFile: true,
                                    message: message
                                }, function(success, result) {

                                    var s = result;

                                    if(result['link'] != null)
                                        window.location = result['link'];

                                    if(responseType == 'html' && result['content']) {
                                        $(responseOn).html(result['content'])
                                        return ;
                                    }

                                    if(result.success) {

                                    }

                                });
                            }

                            count++;
                        });
                    })
                }


            } else {
                berkaPhpJs.request({
                    url: url,
                    type: type,
                    data: formdata ? formdata : form.serialize(),
                    hasFile: true,
                    message: message
                }, function(success, result) {

                    var s = result;

                    if(result['link'] != null)
                        window.location = result['link'];

                    if(responseType == 'html' && result['content']) {
                        $(responseOn).html(result['content'])
                        return ;
                    }

                    if(result.success) {

                    }

                });
            }


        })
    });

};


mts.initSearch = function() {
    $('[data-select]').each(function() {
        var curentElement = $(this);
        $(this).searchselect({
            spinner:'Views/Client/Assets/spinner.gif',
            source: function(searchText, callback) {
                $.get('/client/ajax/type/' + curentElement.data('type')+'', function(data) {
                    data = $.parseJSON(data);
                    callback(data);
                });
            },
            onItemMap: function(item) {

                var word = curentElement.data('text');
                var words = word.split('-');
                var label = '';

                var obj = { value: item[curentElement.data('value')]};

                if(words.length > 0)
                {
                    for(var i= 0; i< words.length; i++){
                        label = label + ' ' + item[words[i]]
                    }

                } else
                {
                    label = item[word];
                }

                obj['label'] = label;

                return obj
            },
            selected: {
                id:curentElement.data('select'),
                text:curentElement.data('select-text')
            }
            ,
            searchable: true,
            onItemSelect: function(item){
                console.log(item);
            }


        })
    })
};

mts.initSearchUser = function() {
    $('[data-search-sender]').each(function() {

        var curentElement = $(this);
        $(this).searchselect({
            source: function(searchText, callback) {
                $.get('/client/ajax/type/Users', function(data) {
                    data = $.parseJSON(data);
                    callback(data);
                });
            },
            onItemMap: function(item) {
                return { value: item['UserID'], label: item['FirstName'] + ' ' + item['LastName']};
            }
            ,
            searchable: true,
            selected: {
                id:null,
                text:null
            },
            onItemSelect: function(item){
                console.log(item);
                $('#SenderName').val(item['FirstName']);
                $('#SenderSurname').val(item['LastName']);
                $('#SenderTel').val(item['Phone']);
                $('#SenderEmail').val(item['EmailAddress']);
                $('#SenderPhysicalAddress').text(item.PhysicalAddress);

                $('#sender-info').removeClass('hide');
            }

        })
    });

    $('[data-search-receiver]').each(function() {

        $(this).searchselect({
            source: function(searchText, callback) {
                $.get('/client/ajax/type/Users', function(data) {
                    data = $.parseJSON(data);
                    callback(data);
                });
            },
            onItemMap: function(item) {
                return { value: item['UserID'], label: item['FirstName'] + ' ' + item['LastName']};
            }
            ,
            searchable: true,
            onItemSelect: function(item){
                console.log(item);
                $('#ReceiverName').val(item['FirstName']);
                $('#ReceiverSurname').val(item['LastName']);
                $('#ReceiverTel').val(item['Phone']);
                $('#ReceiverEmail').val(item['EmailAddress']);

                $('#ReceiverPhysicalAddress').val(item.PhysicalAddress);
                $('#receiver-info').removeClass('hide');

            }

        })
    });
};

mts.generatePin = function(callback) {

    berkaPhpJs.request({
        url:'/client/setting/generatepin',
        message: 'Generating pin...',
        showLoader: false
    }, function(success, result) {
        if(result.success) {
            callback(result['pin']);
        }
    });

};


mts.initTransaction = function() {

    $('#ExistingSender').on('input', function () {
        if($(this).val() == 'YES') {
            $('#SenderSearchID').removeAttr('disabled');
        } else {
            $('#SenderSearchID').attr('disabled', true);
        }
    });

    $('#ExistingReceiver').on('input', function () {
        if($(this).val() == 'YES') {
            $('#ReceiverSearchID').removeAttr('disabled');
        } else {
            $('#ReceiverSearchID').attr('disabled', true);
        }
    });

    $('#btnNewPin').on('click', function(e) {
        mts.generatePin(function(pin) {
            $('#TransactionPIN').val(pin);
        });
    });

    $('#TransactionAmount').on('input', function() {
        var amount = $(this).val();

        if(amount == "")
            amount = 0;
        var interestRate = 0
        $('[data-transaction-amount]').text(amount);

        if(interestRate == "")
            interestRate = 0;

        interestRate = parseFloat(interestRate);
        amount = parseFloat(amount);
        var interest = parseFloat((interestRate / 100 ) * amount).toFixed(2);
        var amountDue = parseFloat((amount + parseFloat(interest))).toFixed(2);

        $('[data-total-amount]').text(amountDue);
        $('[data-interest-amount]').text(interest);


    });

    mts.initSearchUser();
}

mts.initReceive = function()  {

    function confirm(id, authCode, message, uniqueID) {
        berkaPhpJs.request({
            url: '/company/transactions/confirmation',
            type: 'POST',
            data: {transactionAuthorizationCode:authCode, confirmationPIN:id, TransactionUniqueID:uniqueID},
            message: message
        }, function(success, result) {

            if(result.success) {
                $('[data-invoice-body]').html(result['content']);
                $('#invoiceModal').modal('show');
            }

        });
    }

    $('#btnConfirm').on('click', function() {

        var msg =  $(this).attr('message');
        var id = $('#confirmationPIN').val();
        var authCode = $('#transactionAuthCode').val();
        var unique = $('#unique').val();

        $('#confirmationPIN').val('');
        $('#transactionAuthCode').val('');

        confirm(id, authCode, msg, unique)

    });
};

mts.initProof = function() {
    $('[data-print-proof]').on('click', function() {
        $('.print-menu').addClass('hidden');
        window.print();
        $('.print-menu').removeClass('hidden');
    });
};

mts.initAction = function() {
    $('[data-action]').on('click', function() {

        berkaPhpJs.request({
            url: $(this).data('action'),
            type: 'GET',
            message: $(this).attr('message')
        }, function(success, result) {

            if(result.success) {
                $('.action-wrapper').html(result['content']);
                $('#actionModal').modal('show');
                //$('#actionModal').removeClass('fade');
            }

        });

    });


};

mts.initActionButton = function() {
    $('[data-action-btn]').each(function(){

        $(this).confirmation({
            onClick: function(id, event) {

                var url = event.target.getAttribute('data-action-btn');

                if (id == 'yes') {
                    berkaPhpJs.request({
                        url: url,
                        type: 'GET',
                        message: $(this).attr('loading')
                    }, function(success, result) {
                        if(success && result.redirect) {
                            window.location = result.redirect;
                        }
                    });
                }
            }
        });

    })
};

mts.initPlugin = function() {
    $('[data-color-picker]').each(function(){
        $(this).colorPicker();
    })
    $('[data-editor]').each(function () {
        CKEDITOR.replace($(this).attr('id'))
    })
};

mts.initAll = function() {

    berkaPhpJs.init();
    berkaPhpJs.initEqualizer('[data-eq]');
    mts.initFormRequest();
    mts.initSearch();
    mts.initPlugin();
    mts.initTable();
    mts.initCropper();
    mts.initActionButton();


};

var countOrder = 0;

mts.initTable = function() {


    //$("#dataTable").tableExport({
    //    headings: true,
    //    footers: true,
    //    formats: [],
    //    fileName: "id",
    //    bootstrap: true,
    //    position: "bottom",
    //    ignoreRows: null,
    //    ignoreCols: null,
    //    trimWhitespace: true
    //});

    $('#dataTable').DataTable({
        "order": []
    });


};

mts.initCropper = function () {

    if($('[data-image-cropper]').length > 0) {
        var images = [];
        var field = {};

        function readFile(input, preview) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {

                    $(preview).croppie('bind', {
                        url: e.target.result
                    });
                    $('#productImageModel').modal('show');

                };
                reader.readAsDataURL(input.files[0]);
            }
        }



        $('[data-crop-btn]').on('click', function () {


        });

        if ($('[data-image-cropper]').length > 0) {

            $('[data-image-cropper]').each(function () {

                $($(this).data('image-cropper')).croppie({
                    viewport: {
                        width: 360,
                        height: 240
                        //type: 'square'
                    },
                    enableOrientation: true,
                    zoom:0,
                    showZoomer: true,
                    boundary: {
                        width: 450,
                        height: 300
                    }
                });

                $(this).change(function () {
                   // $('[data-crop-btn]').data('crop-btn', $(this).data('file-select'));
                    readFile(this, $(this).data('image-cropper'));
                });
            })

        }
    }
}



$(document).ready(function() {
    mts.initAll();
});