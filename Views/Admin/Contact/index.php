
<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="panel-title">
                <div class="title-box">
                    <label class="text-muted ">Contact</label>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <form data-toggle="validator" message="Updating..." request-type="POST" id="formUser" data-request="<?= BerkaPhp\Helper\Html::action('/contact/edit/'.$contact->id)?>">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="mobileNumber">Mobile:</label>
                        <div class="input-group">
                            <input required type="text" class="form-control" name="mobileNumber" id="mobileNumber" value="<?=$contact->mobileNumber?>">
                            <div class="input-group-addon">
                                <i class="fa fa-phone"></i>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <div class="input-group">
                            <input required type="text" class="form-control" name="email" id="email" value="<?=$contact->email?>">
                            <div class="input-group-addon">
                                <i class="glyphicon glyphicon-envelope"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="facebook">Facebook:</label>
                        <div class="input-group">
                            <input required type="text" class="form-control" name="facebook" id="facebook" value="<?=$contact->facebook?>">
                            <div class="input-group-addon">
                                <i class="fa fa-facebook"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="twitter">Twitter:</label>
                        <div class="input-group">
                            <input required type="text" class="form-control" name="twitter" id="twitter" value="<?=$contact->twitter?>">
                            <div class="input-group-addon">
                                <i class="fa fa-twitter"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="physicalAddress">Physical Address:</label>
                        <textarea required class="form-control" name="physicalAddress" id="physicalAddress"><?=$contact->physicalAddress?></textarea>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="map">Map:</label>
                        <textarea rows="5" required class="form-control" name="map" id="map"><?=$contact->map?></textarea>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="feedback">Feedback:</label>
                        <textarea required class="form-control" name="feedback" id="feedback"><?=$contact->feedback?></textarea>
                    </div>
                </div>
            </div>
            <input type="hidden" id="id" name="id" value="<?=$contact->id?>"/>
            <button type="submit" class="btn btn-success">Update </button>
        </form>
    </div>
</div>