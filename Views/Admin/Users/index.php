
<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="title-box">
                <label class="text-muted pull-left"><span class="badge title">Users</span></label>
                <div class="btn-group pull-right">
                    <a href="<?= BerkaPhp\Helper\Html::action('/users/add') ?>" type="button" class="btn btn-default">
                        New User
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table" id="dataTable">
                <thead class="thead-inverse">
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Mobile Number</th>
                        <th>Username</th>
                        <th>Phone verification</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($users as $user) :?>
                    <tr>
                        <td><?=$user->name?></td>
                        <td><?=$user->surname?></td>
                        <td><?=$user->mobileNumber?></td>
                        <td><?=$user->username?></td>
                        <td><?=($user->isPhoneNumberConfirmed == 1 ? 'Yes' : 'Not Yet')?></td>
                        <td><?=$user->status->name?></td>
                        <td>
                            <a href="<?= BerkaPhp\Helper\Html::action('/users/edit/'.$user->id) ?>">
                                <span class="label label-default">Manage User</span>
                            </a>
                        </td>
                    </tr>
                <?php endforeach?>
                </tbody>
            </table>
        </div>
    </div>
</div>