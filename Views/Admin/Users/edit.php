
<div class="content-box-large">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="title-box">
                <label class="text-muted pull-left"><span class="badge title">Updating <?=$user->name?></span></label>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <form data-toggle="validator"   message="Updating <?=$user->name?>..." request-type="POST" id="formUser" data-request="<?= BerkaPhp\Helper\Html::action('/users/edit/'.$user->id)?>">
            <div class="row">

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input required type="text" class="form-control" name="name" id="name" value="<?=$user->name?>">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="surname">Surname:</label>
                        <input required type="text" class="form-control" name="surname" id="surname" value="<?=$user->surname?>">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="mobileNumber">Mobile Number:</label>
                        <input required type="text" class="form-control" name="mobileNumber" id="mobileNumber" value="<?=$user->mobileNumber?>">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="f">Temporary Password</label>
                        <input type="password" disabled class="form-control" value="<?=$user->password?>">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="refRoleId">Role</label>
                        <?= Util\Helper::select('refRoleId', $roles, ['selected'=>$user->refRoleId,'value'=>'id', 'text'=>'name', 'class'=>'form-control']) ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="refStatusId">Status</label>
                        <?= Util\Helper::select('refStatusId', $status, ['selected'=>$user->refStatusId, 'value'=>'id', 'text'=>'name', 'class'=>'form-control']) ?>
                    </div>
                </div>
            </div>
            <input type="hidden" id="id" name="id" value="<?=$user->id?>"/>
            <button type="submit" class="btn btn-success">Update </button>
        </form>
    </div>
</div>