<!DOCTYPE html>
<html lang="en">
<head>

    <?=$metaData ?>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="We are a consulting firm that provides lean thinking culture and manage project
implementation to help organizations achieve high performance and grow their
business profit">
    <meta name="author" content="Asikhule Business Solution">
    <meta name="keywords" content="Lean Manufacturing, Lean Manufacturing">
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta name="robots" content="all">

<!--    <link rel="stylesheet" href="/Views/Client/Layout/css/bootstrap.min.css">-->
    <title><?= empty($title) ? 'AB Solution | Asikhulebusiness.com' : ucfirst($title) ?></title>
<!--    <link rel="stylesheet" href="/Views/Client/Layout/css/customer.css" />-->
    <link rel="shortcut icon" href="/Views/Client/Layout/Template/assets/images/999-logo-icon.png" type="image/x-icon">

<!--    <link rel="stylesheet" href="/Views/Client/Layout/css/styles-merged.css">-->
<!--    <link rel="stylesheet" href="/Views/Client/Layout/css/style.min.css">-->
<!--    <link rel="stylesheet" href="/Views/Client/Layout/css/custom.css">-->

    <link href="/Views/Client/Layout/Template/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="/Views/Client/Layout/Template/assets/css/fontawesome.css">
    <link rel="stylesheet" href="/Views/Client/Layout/Template/assets/css/templatemo-574-mexant.css">
    <link rel="stylesheet" href="/Views/Client/Layout/Template/assets/css/owl.css">
    <link rel="stylesheet" href="/Views/Client/Layout/Template/assets/css/animate.css">
    <link rel="stylesheet" href="/Views/Client/Layout/Template/assets/css/swiper-bundle.min.css">
</head>
<body>
<div class="">
    {content}
</div>

<!-- ***** Header Area Start ***** -->
<header class="header-area header-sticky">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="main-nav">
                    <!-- ***** Logo Start ***** -->
                    <a href="index.html" class="logo">
<!--                        <img src="/Views/Client/Layout/Template/assets/images/999-logo-icon.png" alt="">-->
                    </a>
                    <!-- ***** Logo End ***** -->
                    <!-- ***** Menu Start ***** -->
                    <ul class="nav">
                        <li class="scroll-to-section"><a href="#top" class="active">Home</a></li>
                        <li class="scroll-to-section"><a href="#services">Services</a></li>
                        <li class="scroll-to-section"><a href="#about">About</a></li>
                        <li class="scroll-to-section hide"><a href="#testimonials">Testimonials</a></li>
                        <li class="scroll-to-section"><a href="#contact-us" class="themeBg">Contact Us</a></li>
                    </ul>
                    <a class='menu-trigger'>
                        <span>Menu</span>
                    </a>
                    <!-- ***** Menu End ***** -->
                </nav>
            </div>
        </div>
    </div>
</header>
<!-- ***** Header Area End ***** -->

<!-- ***** Main Banner Area Start ***** -->
<div class="swiper-container" id="top">
    <div class="swiper-wrapper">
        <div class="swiper-slide">
            <div class="slide-inner" style="background-image:url('/Views/Client/Layout/Template/assets/images/slide1.png')">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="header-text">
                                <h2 class="titleSlide"><em>Asikhule</em> Business <em>Solution</em></h2>
                                <div class="div-dec"></div>
                                <h4 class="subTitleHomeSlide"><b class="focus">We Value your people</b>, Your Vision Our Goal, We Drive Result.</h4>
                                <p class="subTitleDescriptionHomeSlide"><b class="sub">We empower your people with the appropriate tools and attitude to achieve goals and sustain results.</b>
                                    With our team of professionals, we make it our goal to help you reach your vision.
                                    We guarantee measurable and positive results after implementing solutions to
                                    your business.
                                </p>
                                <div class="buttons">
                                    <div class="green-button">
                                        <a href="#testimonials">Testimonials</a>
                                    </div>
                                    <div class="orange-button scroll-to-section">
                                        <a href="#contact-us" class="themeBg">Contact Us</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="slide-inner" style="background-image:url('/Views/Client/Layout/Template/assets/images/slide-01.jpg')">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="header-text">
                                <h2 class="titleSlide"><em>Asikhule</em> Business <em>Solution</em></h2>
                                <div class="div-dec"></div>
                                <h4 class="subTitleHomeSlide">We Value your people, <b class="focus">Your Vision Our Goal</b>, We Drive Result.</h4>
                                <p class="subTitleDescriptionHomeSlide">We empower your people with the appropriate tools and attitude to achieve goals and sustain results.
                                    <b class="sub">With our team of professionals, we make it our goal to help you reach your vision.</b>
                                    We guarantee measurable and positive results after implementing solutions to
                                    your business.
                                </p>
                                <div class="buttons">
                                    <div class="green-button">
                                        <a href="#testimonials">Testimonials</a>
                                    </div>
                                    <div class="orange-button scroll-to-section">
                                        <a href="#contact-us" class="themeBg">Contact Us</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="slide-inner" style="background-image:url('/Views/Client/Layout/Template/assets/images/slide-02.jpg')">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="header-text">
                                <h2 class="titleSlide"><em>Asikhule</em> Business <em>Solution</em></h2>
                                <div class="div-dec"></div>
                                <h4 class="subTitleHomeSlide">We Value your people, Your Vision Our Goal, <b class="focus">We Drive Result.</b></h4>
                                <p class="subTitleDescriptionHomeSlide">We empower your people with the appropriate tools and attitude to achieve goals and sustain results.
                                    With our team of professionals, we make it our goal to help you reach your vision.
                                    <b class="sub">We guarantee measurable and positive results after implementing solutions to
                                    your business.</b>
                                </p>
                                <div class="buttons">
                                    <div class="green-button">
                                        <a href="#testimonials">Testimonials</a>
                                    </div>
                                    <div class="orange-button scroll-to-section">
                                        <a href="#contact-us" class="themeBg">Contact Us</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="swiper-button-next swiper-button-white"></div>
    <div class="swiper-button-prev swiper-button-white"></div>
</div>

<!-- ***** Main Banner Area End ***** -->

<section class="about-us" id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="section-heading headerSection mobile">
                    <h4 class="">About Us</h4>
                    <br>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="right-content">
                    <p class="paragraph">
                        We are a consulting firm that provides lean thinking culture and manage project implementation to help organizations achieve high performance and grow their business profit.
                    </p>
                    <h4 class="headerSection">What we do</h4>
                    <div class="listHolder">
                        <p class="paragraph">
                            We enable you to achieve success through the development of your team and culture with trainings that will strengthen their competencies and equip them with the knowledge and tools to effectively execute their responsibilities.
                        </p>
                        <p class="paragraph">
                            We identify operational and organisational problems through lean thinking and provide sustainable solutions which give our clients the ability to grow and build a competitive advantage.
                        </p>
                        <p class="paragraph">
                            We coordinate and successfully execute projects for our clients.
                        </p>
                    </div>
                    <div class="green-button hide">
                        <a href="about-us.html">Discover More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="services" id="services">
    <div class="container">
        <div class="row">

            <div class="col-lg-6 offset-lg-3">
                <div class="section-heading headerSection">
                    <h4 class="">Our Services</h4>
                    <br>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="service-item">
                    <i class="fas fa-hands-helping"></i>
                    <h4>Training Solutions</h4>
                    <p class="serviceParagraph">We provide a range of classroom and online trainings that enable you to gain operational excellence, continuous improvement, customer satisfaction and management skills.</p>
                    <p class="serviceParagraph">
                        <br>Types of training :
                        Lean Manufacturing, Project Management, Supervisory skills and Management skills
                        <br><br>
                    </p>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="service-item">
                    <i class="fas fa-business-time"></i>
                    <h4>Business Improvement</h4>
                    <p class="serviceParagraph">
                        We apply lean six sigma tools and other business process improvement techniques to identify your challenges and implement solutions which optimize your processes, reduce cost, and improve quality as well as delivery performance.
                    </p>

                    <p class="serviceParagraph">
                        <br>We also identify opportunities to maximize business profit.
                        Problem Solving, Continuous Improvement, Process Deployment and Control
                    </p>

                </div>
            </div>
            <div class="col-lg-12">
                <div class="service-item">
                    <i class="fas fa-project-diagram"></i>
                    <h4>Project Management</h4>
                    <p class="serviceParagraph">From the customer vision, we perform a feasibility study by collecting and analysing data that will help establish a well-defined business case and execute the project within time and cost allocated.</p>

                    <p class="serviceParagraph">
                        <br>Research development, Project conception, Project coordination and execution, Project documentation
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contact-us contactus" id="contact-us">

    <section class="map">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="section-heading">
                        <h4 class="headingTopSpace">Contact US</h4>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div id="map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d27660.675385018836!2d28.16880254871049!3d-25.76171616100374!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1e956188772d8987%3A0xff24c30b6145b83c!2sMuckleneuk%2C%20Pretoria%2C%200002!5e1!3m2!1sen!2sza!4v1697390550886!5m2!1sen!2sza" width="100%" height="450px" style="border:0; border-radius: 5px; position: relative; z-index: 2;" allowfullscreen=""></iframe>
                    </div>
                </div>
                <div class="col-lg-10 offset-lg-1">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="info-item">
                                <i class="fa fa-envelope"></i>
                                <h4>Email Address</h4>
                                <a href="#">admin@asikhulebusiness.com</a>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="info-item">
                                <i class="fa fa-phone"></i>
                                <h4>Phone Number</h4>
                                <a href="#">+27 739 487 789 / +27 719229515</a>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="info-item">
                                <i class="fa fa-map-marked-alt"></i>
                                <h4>Address</h4>
                                <a target="_blank" href="https://maps.app.goo.gl/QcqzFmHX28aJHgde6">Muckleneuck, 002 Pretoria</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>

    <section class="contact-us-form">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="section-heading">
                        <h6>Contact Us</h6>
                        <h4>Feel free to message us</h4>
                    </div>
                </div>
                <div class="col-lg-10 offset-lg-1">
                    <form id="contact"  data-toggle="validator" message="Sending message..." request-type="POST"  data-request="/contact/message">
                        <div class="row">
                            <div class="col-lg-6">
                                <fieldset>
                                    <input type="name" name="name" id="name" placeholder="Your Name..." autocomplete="on" required>
                                </fieldset>
                            </div>
                            <div class="col-lg-6">
                                <fieldset>
                                    <input type="phone" name="phone" id="phone" placeholder="Your Phone..." autocomplete="on" required>
                                </fieldset>
                            </div>
                            <div class="col-lg-6">
                                <fieldset>
                                    <input type="text" name="email" id="email" pattern="[^ @]*@[^ @]*" placeholder="Your E-mail..." required="">
                                </fieldset>
                            </div>
                            <div class="col-lg-6">
                                <fieldset>
                                    <input type="subject" name="subject" id="subject" placeholder="Subject..." autocomplete="on" >
                                </fieldset>
                            </div>
                            <div class="col-lg-12">
                                <fieldset>
                                    <textarea name="message" id="message" placeholder="Your Message"></textarea>
                                </fieldset>
                            </div>
                            <div class="col-lg-12">
                                <fieldset>
                                    <button type="submit" id="form-submit" class="orange-button">Send Message</button>
                                </fieldset>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <br>
        <br>
    </section>

</section>

<section class="testimonials" id="testimonials">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="section-heading">
                    <h4>What They Say</h4>
                </div>
            </div>
            <div class="col-lg-10 offset-lg-1">
                <div class="owl-testimonials owl-carousel" style="position: relative; z-index: 5;">
                    <div class="item">
                        <i class="fa fa-quote-left"></i>
                        <p>“Your consultation company demonstrates a strong understanding of the complexities involved in construction projects, providing valuable insights and guidance..”</p>
                        <h4>Berka Ayowa</h4>
                        <span>CEO of SoftclickTech (Pty) Ltd</span>
                        <div class="right-image">
                            <img src="/Views/Client/Layout/Template/assets/images/img.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<br>
<br>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p>Copyright © 2023 AB Solution Portfolio., Ltd. All Rights Reserved. <br>
                    <a rel="sponsored" href="https://www.asikhulebusiness.com" target="_blank">www.asikhulebusiness.com</a></p>
            </div>
        </div>
    </div>
</footer>

<!-- Scripts -->
<!-- Bootstrap core JavaScript -->
<script src="/Views/Client/Layout/Template/vendor/jquery/jquery.min.js"></script>
<script src="/Views/Client/Layout/Template/vendor/bootstrap/js/bootstrap.min.js"></script>

<script src="/Views/Client/Layout/Template/assets/js/isotope.min.js"></script>
<script src="/Views/Client/Layout/Template/assets/js/owl-carousel.js"></script>

<script src="/Views/Client/Layout/Template/assets/js/tabs.js"></script>
<script src="/Views/Client/Layout/Template/assets/js/swiper.js"></script>
<script src="/Views/Client/Layout/Template/assets/js/custom.js"></script>
<script>
    var interleaveOffset = 0.5;

    var swiperOptions = {
        loop: true,
        speed: 1000,
        grabCursor: true,
        watchSlidesProgress: true,
        mousewheelControl: true,
        keyboardControl: true,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev"
        },
        on: {
            progress: function() {
                var swiper = this;
                for (var i = 0; i < swiper.slides.length; i++) {
                    var slideProgress = swiper.slides[i].progress;
                    var innerOffset = swiper.width * interleaveOffset;
                    var innerTranslate = slideProgress * innerOffset;
                    swiper.slides[i].querySelector(".slide-inner").style.transform =
                        "translate3d(" + innerTranslate + "px, 0, 0)";
                }
            },
            touchStart: function() {
                var swiper = this;
                for (var i = 0; i < swiper.slides.length; i++) {
                    swiper.slides[i].style.transition = "";
                }
            },
            setTransition: function(speed) {
                var swiper = this;
                for (var i = 0; i < swiper.slides.length; i++) {
                    swiper.slides[i].style.transition = speed + "ms";
                    swiper.slides[i].querySelector(".slide-inner").style.transition =
                        speed + "ms";
                }
            }
        }
    };

    var swiper = new Swiper(".swiper-container", swiperOptions);
</script>

<div id="google_translate_element"></div>
<script type="text/javascript">// <![CDATA[
    function googleTranslateElementInit() {
        new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'fr, en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
    }
    // ]]></script>
<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit" type="text/javascript"></script>
</body>
</html>



<!--<script src="/Views/Admin/Layout/js/jquery-2.1.1.js"></script>-->
<!--<script src="/Views/Client/Layout/js/bootstrap.min.js"></script>-->
<!---->
<!--<script src="/Views/Client/Layout/js/scripts.min.js"></script>-->
<!--<script src="/Views/Client/Layout/js/main.min.js"></script>-->
<!--<script src="/Views/Client/Layout/js/custom.js"></script>-->
<!--<script src="/Views/Admin/Layout/js/Jquery_table.js"></script>-->
<!--<script src="/Views/Admin/Layout/js/datatables.js"></script>-->
<script src="/Views/Admin/Layout/js/notification.js"></script>
<script src="/Views/Admin/Layout/js/loader.js"></script>
<script src="/Views/Admin/Layout/js/confirmation.js"></script>
<script src="/BerkaPhp/Template/Utility/Javascript/app.js"></script>
<script src="/Views/Admin/Layout/js/app.js"></script>








