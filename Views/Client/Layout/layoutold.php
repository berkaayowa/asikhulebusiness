<!DOCTYPE html>
<html lang="en">
<head>


    <?=$metaData ?>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="Green Valley Design is recognized as one of the leading installers and maintaining of high quality artificial grass in South Africa.">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta name="robots" content="all">

<!--    <link rel="stylesheet" href="/Views/Client/Layout/css/bootstrap.min.css">-->
    <title><?= empty($title) ? 'Asikhule Business Solution Portfolio' : ucfirst($title) ?></title>
    <link rel="stylesheet" href="/Views/Client/Layout/css/customer.css" />
    <link rel="shortcut icon" href="/Views/Asset/icon.png" type="image/x-icon">

    <link rel="stylesheet" href="/Views/Client/Layout/css/styles-merged.css">
    <link rel="stylesheet" href="/Views/Client/Layout/css/style.min.css">
    <link rel="stylesheet" href="/Views/Client/Layout/css/custom.css">

    <link href="/Views/Client/Layout/Template/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    
    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="/Views/Client/Layout/Template/assets/css/fontawesome.css">
    <link rel="stylesheet" href="/Views/Client/Layout/Template/assets/css/templatemo-574-mexant.css">
    <link rel="stylesheet" href="/Views/Client/Layout/Template/assets/css/owl.css">
    <link rel="stylesheet" href="/Views/Client/Layout/Template/assets/css/animate.css">
    <link rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css">
</head>
<body>
<div class="probootstrap-loader"></div>
<header role="banner" class="probootstrap-header">
    <div class="container">
        <a href="index.html" class="probootstrap-logo">
            <div class="logo">
                <img src="/Views/Asset/logo3.png" class="logo-img" alt=""/>
            </div>
        </a>

        <a href="#" class="probootstrap-burger-menu visible-xs" ><i>Menu</i></a>
        <div class="mobile-menu-overlay"></div>

        <nav role="navigation" class="probootstrap-nav hidden-xs">
            <ul class="probootstrap-main-nav">
                <li class="active"><a href="/">Home</a></li>
                <li><a href="/services">Our Services</a></li>
                <li><a href="/aboutus">About</a></li>
                <li><a href="/gallery">Gallery</a></li>
                <li><a href="/contact">Contact</a></li>
                <?php if(\BerkaPhp\Helper\Auth::IsUserLogged()): ?>
                    <li><a href="/admin/users"><i class="icon icon-tools"></i> Admin Panel</a></li>
                    <li><a href="/users/logout"><i class="icon icon-log-out"></i> Logout</a></li>
                <?php endif ?>
            </ul>
            <div class="extra-text visible-xs">
                <a href="#" class="probootstrap-burger-menu"><i>Menu</i></a>
                <h5>Connect</h5>
                <ul class="social-buttons">
                    <li><a href="#"><i class="icon-twitter"></i></a></li>
                    <li><a href="#"><i class="icon-facebook2"></i></a></li>
                    <li><a href="#"><i class="icon-instagram2"></i></a></li>
                </ul>
            </div>
        </nav>
    </div>
</header>
<!-- END: header -->
<section class="probootstrap-slider flexslider">
    <div class="probootstrap-text-intro">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="probootstrap-slider-text text-center">
                        <h1 class="probootstrap-heading probootstrap-animate mb20" data-animate-effect="fadeIn">

                        </h1>
                        <div class="probootstrap-animate probootstrap-sub-wrap mb30" data-animate-effect="fadeIn">
                            <a href="https://uicookies.com/">Green</a> Valley <a href="https://uicookies.com/license/">Design</a>
                        </div>
                        <p class="probootstrap-animate" data-animate-effect="fadeIn">
                            <a href="#" class="btn btn-ghost btn-ghost-white" style="font-weight: bolder;">
                                <?=$menuTitle?>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <ul class="slides">
        <li style="background-image: url('/Views/Asset/Images/artificial-grass-5.jpg');"></li>
        <li style="background-image: url('/Views/Asset/Images/artificial-grass-4.jpg');"></li>
        <li style="background-image: url('/Views/Asset/Images/artificial-grass-3.jpg');"></li>
        <li style="background-image: url('/Views/Asset/Images/artificial-grass-01.jpg');"></li>
        <li style="background-image: url('/Views/Asset/Images/artificial-grass-2.jpg');"></li>
    </ul>
</section>
<!-- END: slider  -->
<div class="">
    {content}
</div>

<footer class="probootstrap-footer probootstrap-bg" style="background-image: url('/Views/Asset/Images/slider_3.jpg')">
    <div class="container">
        <div class="row mb60">
            <div class="col-md-4">
                <div class="probootstrap-footer-widget">
                    <h4 class="heading">About Green Valley Design</h4>
                    <p><?=BrkORM\T::Find('about')->Where('isDeleted', '=', \Helper\Check::$False)->FetchFirstOrDefault()->summary?></p>
                    <p><a href="/aboutus">Read more...</a></p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="probootstrap-footer-widget">
                    <h4 class="heading">Products</h4>
                    <ul class="stack-link">
                        <?php
                            $products = BrkORM\T::Find('service')->Where('isDeleted', '=', \Helper\Check::$False)->FetchList();
                            $contact = BrkORM\T::Find('contact')->Where('isDeleted', '=', \Helper\Check::$False)->FetchFirstOrDefault();
                        ?>
                        <li>
                        <?php foreach($products as $product) : ?>
                            <a href="/service/view/<?=$product->id?>/<?=$product->name?>">
                                <?=$product->name?>,
                            </a>
                        <?php endforeach ?>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="probootstrap-footer-widget">
                    <h4 class="heading">Subscribe</h4>
                    <p>To receive our latest news</p>
                    <form action="#">
                        <div class="form-field">
                            <input type="text" class="form-control" placeholder="Enter your email">
                            <button class="btn btn-subscribe"><i class="icon-arrow-long-right"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row copyright">
            <div class="col-md-6">
                <div class="probootstrap-footer-widget">
                    <p>&copy; 2018 <a href="https://uicookies.com/">Green Valley Design</a> |
                        <?php if(\BerkaPhp\Helper\Auth::IsUserLogged()): ?>
                            <a href="/admin/users">Hi <?= ucfirst(\BerkaPhp\Helper\Auth::GetActiveUser()->name)?></a> | <a href="/users/logout">logout</a>
                        <?php else :?>
                            <a href="/users/login">Login</a>
                        <?php endif ?>
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="probootstrap-footer-widget right">
                    <ul class="probootstrap-footer-social">
                        <li><a href="<?=$contact->twitter?>"><i class="icon-twitter"></i></a></li>
                        <li><a href="<?=$contact->facebook?>"><i class="icon-facebook"></i></a></li>
                        <li><a href="mailto:<?=$contact->email?>"><i class="icon-envelop"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-chevron-thin-up"></i></a>
</div>

</body>
</html>

<script src="/Views/Admin/Layout/js/jquery-2.1.1.js"></script>
<script src="/Views/Client/Layout/js/bootstrap.min.js"></script>

<script src="/Views/Client/Layout/js/scripts.min.js"></script>
<script src="/Views/Client/Layout/js/main.min.js"></script>
<script src="/Views/Client/Layout/js/custom.js"></script>
<script src="/Views/Admin/Layout/js/Jquery_table.js"></script>
<script src="/Views/Admin/Layout/js/datatables.js"></script>
<script src="/Views/Admin/Layout/js/notification.js"></script>
<script src="/Views/Admin/Layout/js/loader.js"></script>
<script src="/Views/Admin/Layout/js/confirmation.js"></script>
<script src="/BerkaPhp/Template/Utility/Javascript/app.js"></script>
<script src="/Views/Admin/Layout/js/app.js"></script>






