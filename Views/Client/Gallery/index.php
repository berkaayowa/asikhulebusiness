<section class="probootstrap-section">
    <div class="container">
        <div class="probootstrap-section-heading text-center mb50 probootstrap-animate">
            <h4 class="sub-heading">Our Gallery</h4>
            <h2 class="heading">See &amp; Explore</h2>
        </div>
        <div class="row">
            <?php foreach($images as $image) : ?>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 probootstrap-animate" data-animate-effect="fadeIn">
                    <a href="<?= $image->image != null && !file_exists('Views/Asset/Images/'.$image->image) ? '/Views/Asset/Images/slider_4.jpg' : '/Views/Asset/Images/'.$image->image?>" class="image-popup">
                        <img src="<?= $image->image != null && !file_exists('Views/Asset/Images/'.$image->image) ? '/Views/Asset/Images/slider_4.jpg' : '/Views/Asset/Images/'.$image->image?>" alt="Free Template by uicookies.com" class="img-responsive">
                    </a>
                </div>
<!--                <div class="clearfix visible-lg-block visible-sm-block"></div>-->
            <?php endforeach ?>
        </div>
    </div>
</section>


<section class="probootstrap-section probootstrap-bg hidden" style="background-image: url('/Views/Asset/Images/slider_2.jpg');">
    <div class="container text-center">
        <h2 class="heading probootstrap-animate" data-animate-effect="fadeIn">Fresh &amp; Non-GMO Products</h2>
        <p class="sub-heading probootstrap-animate" data-animate-effect="fadeIn">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
    </div>
</section>

<section class="probootstrap-section hidden">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 probootstrap-animate">
                <div class="service">
                    <div class="icon"><i class="icon-leaf"></i></div>
                    <h2 class="heading">Fresh Vegies &amp; Fruits</h2>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                    <p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 probootstrap-animate">
                <div class="service">
                    <div class="icon"><i class="icon-hand"></i></div>
                    <h2 class="heading">Locally Grown Vegetables</h2>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                    <p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                </div>
            </div>
            <div class="clearfix visible-sm-block"></div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 probootstrap-animate">
                <div class="service">
                    <div class="icon"><i class="icon-tree"></i></div>
                    <h2 class="heading">Natural As It's In Nature</h2>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                    <p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                </div>
            </div>
            <div class="clearfix visible-lg-block visible-md-block"></div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 probootstrap-animate">
                <div class="service">
                    <div class="icon"><i class="icon-location"></i></div>
                    <h2 class="heading">From Country Side</h2>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                    <p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                </div>
            </div>
            <div class="clearfix visible-sm-block"></div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 probootstrap-animate">
                <div class="service">
                    <div class="icon"><i class="icon-water"></i></div>
                    <h2 class="heading">Alkaline Water</h2>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                    <p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 probootstrap-animate">
                <div class="service">
                    <div class="icon"><i class="icon-air"></i></div>
                    <h2 class="heading">Fresh Air</h2>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                    <p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                </div>
            </div>
            <div class="clearfix visible-lg-block visible-md-block"></div>
        </div>
    </div>
</section>
