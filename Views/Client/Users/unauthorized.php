<div class="wrapper row">
    <div class="col-md-12">
        <div class="login-form text-center">
            <h2>Unauthorized Access</h2>
            <a href="/users/login" class="btn btn-success"> Back to login</a>
        </div>
    </div>
</div>