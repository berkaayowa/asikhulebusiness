<section class="probootstrap-section">
    <div class="container">
        <div class="probootstrap-section-heading text-center mb50 probootstrap-animate">
            <h4 class="sub-heading">Administration Panel</h4>
            <h2 class="heading">Login</h2>
        </div>
        <div class="wrapper row">
            <div class="col-md-12">
                <div class="login-form">
                    <form data-toggle="validator" message="Login..." request-type="POST" id="formLogin" data-request="/users/login">
                        <div class="form-group">
                            <input type="text" name="username" id="username" class="form-control" placeholder="Username" required="required">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" id="password"  class="form-control" placeholder="Password" required="required">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success btn-block">Log in</button>
                        </div>
                        <div class="clearfix hidden">
                            <label class="pull-left checkbox-inline"><input type="checkbox"> Remember me</label>
                            <a href="#" class="pull-right">Forgot Password?</a>
                        </div>
                    </form>
                    <p class="text-center hidden"><a href="#">Create an Account</a></p>
                </div>
            </div>
        </div>
    </div>
</section>