<section class="probootstrap-section">
    <div class="container">
        <div class="row">
            <?php foreach($services as $service) : ?>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 probootstrap-animate" data-animate-effect="fadeIn">
                    <div class="product-item">
                        <figure>
                            <img src="<?= $service->image != null && !file_exists('Views/Asset/Images/'.$service->image) ? 'Views/Asset/Images/slider_4.jpg' : '/Views/Asset/Images/'.$service->image?>" alt="Free Template by uicookies.com" class="img-responsive">
                        </figure>
                        <div class="text">
                            <h2 class="heading"><?=$service->name?>,</h2>
                            <?=$service->summary?>
                            <p><a href="/service/view/<?=$service->id?>/<?=$service->name?>" class="btn btn-primary btn-sm">View Details</a></p>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
            <div class="clearfix visible-lg-block visible-md-block"></div>
        </div>
    </div>
</section>

<section class="probootstrap-section probootstrap-bg hidden" style="background-image: url('/Views/Asset/Images/slider_2.jpg');">
    <div class="container text-center">
        <h2 class="heading probootstrap-animate" data-animate-effect="fadeIn">Fresh &amp; Non-GMO Products</h2>
        <p class="sub-heading probootstrap-animate" data-animate-effect="fadeIn">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
    </div>
</section>
