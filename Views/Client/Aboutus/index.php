<section class="probootstrap-section">
    <div class="container">
        <div class="probootstrap-section-heading text-center mb50 probootstrap-animate">
            <h4 class="sub-heading">About</h4>
            <h2 class="heading"><?=$about->title?></h2>
        </div>
        <div class="row">
            <div class="col-md-12 probootstrap-animate">
                <?=$about->summary?>
            </div>
            <div class="col-md-12">
                &nbsp;
            </div>
            <div class="col-md-12 probootstrap-animate">
                <?=$about->content?>
            </div>
        </div>
    </div>
</section>